yapf:
	yapf -i ./*.py ./cogs/*.py

clean-log-file:
	rm -f ./*.log

clean-database:
	rm -f ./*.db ./*.db-shm ./*.db-wal

clean-pycache:
	rm -rf ./__pycache__ ./cogs/__pycache__

clean: clean-log-file clean-database clean-pycache

.PHONY: yapf clean-log-file clean-database clean-pycache clean
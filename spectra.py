import asyncio
import json
import logging
import sys
import datetime

import discord
from discord.ext import commands

from db_manager import DBManager
from utility import load_toml_file, logger_config, CONFIG_FILE_NAME

from cogs.event_response import EventResponseModule

if sys.implementation.name == "cpython":
    try:
        import uvloop
        print('*** Using uvloop')
    except ImportError:
        pass
    else:
        uvloop.install()


async def get_prefix(bot, message):
    guild_id = message.channel.guild.id
    prefix = db.select('guilds', 'prefix', 'guild_id=?', (guild_id, ))
    return commands.when_mentioned_or(prefix)(bot, message)


def initialize_globals():
    config = load_toml_file(CONFIG_FILE_NAME, do_logging=False)
    (log_file_format, log_file_name, log_console_format) =  \
        (config['log_file_format'], config['log_file_name'], config['log_console_format'])
    logger = logger_config(log_file_format, log_file_name, log_console_format)
    logger.info('Config: ' + str(config))
    db = DBManager(config['database_name'], setup=True)
    bot = commands.Bot(command_prefix=get_prefix)
    return (bot, db, logger)


(bot, db, logger) = initialize_globals()


def initialize_misc_settings(db):
    config = load_toml_file(CONFIG_FILE_NAME, do_logging=False)
    misc_settings_fields = [
        'setting_name TEXT NOT NULL PRIMARY KEY', 'setting_value NOT NULL'
    ]
    initial_cogs_value = ' '.join(config['initial_cogs'])
    with db.conn:
        db.create_table('misc_settings', misc_settings_fields, rowid=False)
        db.insert('misc_settings', ('database_name', config['database_name']))
        db.insert('misc_settings', ('token', config['token']))
        db.insert('misc_settings', ('initial_cogs', initial_cogs_value))


def create_guilds_table(db):
    fields = ('guild_id INTEGER PRIMARY KEY', 'prefix TEXT NOT NULL')
    db.create_table('guilds', fields)


##### EVENT HANDLERS


async def handle_guild_join(guild):
    DEFAULT_PREFIX = '?'
    with db.conn:
        db.insert('guilds', (guild.id, DEFAULT_PREFIX), on_conflict='IGNORE')
    return


async def handle_on_ready():
    logger.info(f'Logged in as: {bot.user.name} {bot.user.id}')
    create_guilds_table(db)
    for guild in bot.guilds:
        await handle_guild_join(guild)
    return


##### EVENT HANDLERS


def get_triggered_modules(event,
                          guild: discord.Guild,
                          channel: discord.TextChannel = None):
    select_results = db.select(
        'er_modules',
        '(module_name, channel_mode, module_type, response_mode, response_channel)',
        'guild_id=? AND event=?',
        values=(guild.id, event),
        fetchone=False)
    triggered_modules = []
    for result in select_results:
        module = EventResponseModule(*result)
        if channel is None:
            triggered_modules += [module]
            continue
        in_channels_table = db.select_exists(
            'er_channels', 'guild_id=? AND module_name=? AND channel_id=?',
            (guild.id, module.module_name, channel.id))
        if module.channel_mode == 'blacklist' and in_channels_table:
            continue
        elif module.channel_mode == 'whitelist' and not in_channels_table:
            continue
        else:
            triggered_modules += [module]
    return triggered_modules


##### EVENTS


@bot.event
async def on_guild_join(guild):
    await handle_guild_join(guild)
    return


@bot.event
async def on_ready():
    await handle_on_ready()
    return


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    await bot.process_commands(message)
    return

def create_authored_log_message(author, description, **kwargs):
    timestamp = datetime.datetime.utcnow()
    embed = discord.Embed(description=description, timestamp=timestamp, **kwargs)
    embed = embed.set_author(name=author.display_name, icon_url=author.avatar_url)
    embed = embed.set_footer(text=f'User ID: {author.id}')
    return embed


def get_message_edit_embed(before, after):
    desc = f'**Message edited in** {after.channel.mention} [Jump to Message]({after.jump_url})'
    embed = create_authored_log_message(after.author, desc)
    embed = embed.add_field(
        name='**Before:**', value=before.content, inline=False)
    embed = embed.add_field(
        name='**After:**', value=after.content, inline=False)

    return embed


@bot.event
async def on_message_edit(before, after):
    if after.author == bot.user:
        return
    embed = get_message_edit_embed(before, after)
    log_chan = after.guild.get_channel(560443644629483530)
    await log_chan.send(embed=embed)


@bot.event
async def on_reaction_add(reaction, user):
    # if not isinstance(reaction.emoji, str) or reaction.emoji != ':star:':
    #     return
    source_msg = reaction.message
    source_channel = reaction.message.channel
    source_user = reaction.message.author
    header = f'**{reaction.count} stars in** {source_channel.mention}'
    embed_content = f'{source_msg.content}\n[->]({source_msg.jump_url})'
    embed = create_authored_log_message(source_user, embed_content)
    if len(source_msg.embeds) > 0:
        url = source_msg.embeds[0].url
        embed.set_image(url=url)
    starboard_chan = source_channel.guild.get_channel(562208236376293386)
    await starboard_chan.send(header, embed=embed)



##### INITIALIZE BOT


def load_initial_cogs(db):
    initial_cogs = db.select('misc_settings', 'setting_value',
                             'setting_name=?', ('initial_cogs', )).split()
    for cog in initial_cogs:
        try:
            bot.load_extension(cog)
        except Exception as err:
            logger.error(f'Failed to load cog {cog}.', err)


def main(bot, db, logger):
    initialize_misc_settings(db)
    logger.info(f'Discord.py version: {discord.__version__}')
    load_initial_cogs(db)
    token = db.select('misc_settings', 'setting_value', 'setting_name=?',
                      ('token', ))
    bot.run(token, reconnect=True)
    return


if __name__ == '__main__':
    main(bot, db, logger)

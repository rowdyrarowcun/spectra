import logging

import tomlkit
from discord.ext import commands

CONFIG_FILE_NAME = 'config.toml'


def logger_config(log_file_format, log_file_name, log_console_format):
    logging.basicConfig(
        filename=log_file_name, level=logging.INFO, format=log_file_format)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter(log_console_format)
    console.setFormatter(formatter)
    logging.getLogger().addHandler(console)
    logger = logging.getLogger(__name__)
    logger.info('Logger configured')
    return logger


def get_logger():
    logger = logging.getLogger(__name__)
    return logger


def load_toml_file(file_name, do_logging=True):
    try:
        with open(file_name, 'r') as open_file:
            toml_str = open_file.read()
        config = tomlkit.loads(toml_str)
        if do_logging:
            logging.info(f'{file_name} loaded')
        else:
            print(f'{file_name} loaded')
        return config
    except OSError as os_err:
        if do_logging:
            logging.error(f'Error loading {file_name} {repr(os_err)}')
        else:
            print(f'Error loading {file_name} {repr(os_err)}')
        return None


def is_cog_loaded(bot: commands.Bot, cog):
    return cog in bot.cogs.keys()

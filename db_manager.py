import sqlite3

from utility import get_logger, load_toml_file


class DBManager:
    def logger_info_db(self, msg, values=None):
        if not values is None:
            msg += f' | {str(values)}'
        self.logger.info(msg)

    def logger_error_db(self, msg, values=None, err=None):
        if not values is None:
            msg += f' | {str(values)}'
        if not err is None:
            msg += f' | {repr(err)}'
        self.logger.error(msg)

    # base methods
    def execute(self, query, values=()):
        try:
            self.conn.execute(query, values)
            self.logger_info_db(query, values=values)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return False

    def create_table(self,
                     table_name,
                     fields,
                     values=(),
                     if_not_exists=True,
                     rowid=True):
        all_fields = ', '.join(fields)
        if if_not_exists:
            if_not_exists_expr = 'IF NOT EXISTS'
        else:
            if_not_exists_expr = ''
        query = f'CREATE TABLE {if_not_exists_expr} {table_name} ({all_fields})'
        if not rowid:
            query += ' WITHOUT ROWID'
        try:
            self.cursor.execute(query, values)
            self.logger_info_db(query, values=values)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return False

    def insert(self, table_name, values, on_conflict='ABORT'):
        qmarks = ','.join(['?'] * len(values))
        query = f'INSERT OR {on_conflict} INTO {table_name} VALUES ({qmarks})'
        try:
            self.cursor.execute(query, values)
            self.logger_info_db(query, values=values)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return False

    def update(self, table_name, set_expr, search_expr, values=()):
        query = f'UPDATE {table_name} SET {set_expr} WHERE {search_expr}'
        try:
            self.cursor.execute(query, values)
            self.logger_info_db(query, values=values)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return False

    def select(self,
               table_name,
               result_expr,
               search_expr,
               values=(),
               fetchone=True):
        SINGLETON_INDEX = 0
        SINGLETON_LEN = 1
        query = f'SELECT {result_expr} FROM {table_name} WHERE {search_expr}'
        try:
            self.cursor.execute(query, values)
            if fetchone:
                fetched = self.cursor.fetchone()
                if not fetched is None and len(fetched) == SINGLETON_LEN:
                    fetched = fetched[SINGLETON_INDEX]
            else:
                fetched = self.cursor.fetchall()
            self.logger_info_db(query, values=values)
            return fetched
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return None

    def select_exists(self, table_name, search_expr, values=()):
        SINGLETON_INDEX = 0
        query = f'SELECT EXISTS (SELECT 1 FROM {table_name} WHERE {search_expr})'
        try:
            self.cursor.execute(query, values)
            fetched = self.cursor.fetchone()[SINGLETON_INDEX]
            result_str = f' | Result: {fetched}'
            self.logger_info_db(query + result_str, values=values)
            return fetched
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return None

    def delete(self, table_name, search_expr, values=()):
        query = f'DELETE FROM {table_name} WHERE {search_expr}'
        try:
            self.cursor.execute(query, values)
            self.logger_info_db(query, values=values)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, values=values, err=err)
            return False

    def drop_table(self, table_name):
        query = f'DROP TABLE {table_name}'
        try:
            self.cursor.execute(query)
            self.logger_info_db(query)
            return True
        except sqlite3.DatabaseError as err:
            self.logger_error_db(query, err=err)
            return False

    # derived methods

    def table_exists(self, table_name):
        search_expr = 'name=? AND type=?'
        values = (table_name, 'table')
        return self.select_exists('sqlite_master', search_expr, values)

    # initialization / connection management

    def _setup_database(self, database_name):
        all_pragmas = [
            'PRAGMA foreign_keys = ON', 'PRAGMA cache_size = -16384',
            'PRAGMA journal_mode = WAL', 'PRAGMA synchronous = NORMAL'
        ]
        for pragma in all_pragmas:
            self.execute(pragma)
        self.conn.commit()

    def __init__(self, database_name, setup=False):
        self.database_name = database_name
        self.logger = get_logger()
        try:
            self.conn = sqlite3.connect(self.database_name)
            self.cursor = self.conn.cursor()
        except sqlite3.DatabaseError as err:
            self.logger_error_db(
                f'Error during init for DBManager of {self.database_name}',
                err=err)
        if setup:
            self._setup_database(self.database_name)

    def close(self):
        try:
            self.conn.commit()
            self.conn.close()
            msg = f'Connection to Database {self.database_name} closed'
            self.logger_info_db(msg)
            return True
        except sqlite3.DatabaseError as err:
            msg = f'Error when closing connection to Database {self.database_name}'
            self.logger_error_db(msg, err=err)
            return False

    # import / export database


if __name__ == '__main__':
    CONFIG_FILE_NAME = 'config.toml'
    config = load_toml_file(CONFIG_FILE_NAME)
    database_name = config['database_name']
    db = DBManager(database_name)  #pylint: disable=unused-variable

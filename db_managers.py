from db_manager import DBManager


class GuildDB(DBManager):
    def create_guilds_tb(self):
        fields = ['guild_id INTEGER PRIMARY KEY', 'prefix TEXT NOT NULL']
        self.create_table('guilds', fields)

class StarboardMainDBManager(DBManager):
    def create_starboard_main_tb(self):
        fields = [
            'guild_id INTEGER PRIMARY KEY',
            'enabled INTEGER, channel_id INTEGER',
            'selfstar INTEGER, emoji TEXT'
        ]
        self.create_table('starboard_main', fields)


class StarboardBlockedChansDBManager(DBManager):
    def create_starboard_blocked_chans_tb(self):
        fields = [
            'guild_id INTEGER PRIMARY KEY', 'channel_id INTEGER NOT NULL'
        ]
        self.create_table('starboard_blocked_chans', fields)


def main():
    DATABASE_NAME = 'testdb.db'
    db = GuildDB(DATABASE_NAME)
    db.create_guilds_tb()
    db.close()


if __name__ == '__main__':
    main()

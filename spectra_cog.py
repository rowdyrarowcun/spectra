import discord
from discord.ext import commands

from db_manager import DBManager
from utility import get_logger, load_toml_file, CONFIG_FILE_NAME


class SpectraCog(commands.Cog):
    def __init__(self, bot, cog_name):
        self.bot = bot
        config = load_toml_file(CONFIG_FILE_NAME)
        database_name = config['database_name']
        self.db = DBManager(database_name)
        self.cog_name = cog_name
        self.logger = get_logger()
        self.logger.info(f'Initialized {self.cog_name} cog')

import discord
from discord.ext import commands

from spectra_cog import SpectraCog


class LibraryCog(SpectraCog):
    pass


def setup(bot):
    bot.add_cog(LibraryCog(bot, 'Library'))

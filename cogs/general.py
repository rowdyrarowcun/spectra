import discord
from discord.ext import commands

from spectra_cog import SpectraCog


class GeneralCog(SpectraCog):
    @commands.command(name='hello')
    async def _hello_command(self, ctx):
        await ctx.send('Hello!')
        return


def setup(bot):
    bot.add_cog(GeneralCog(bot, 'General'))

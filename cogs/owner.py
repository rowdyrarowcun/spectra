from sys import exit as sys_exit

import discord
from discord.ext import commands

from db_manager import DBManager
from spectra_cog import SpectraCog
from utility import load_toml_file


class OwnerCog(SpectraCog):
    @commands.command(name='changeactivity')
    @commands.is_owner()
    async def _changeactivity_command(self, ctx, activity_text: str = None):
        if activity_text is None:
            new_activity = None
        else:
            new_activity = discord.Game(activity_text)

        self.db.insert('misc_settings', ('activity', activity_text), 'REPLACE')
        status_text = self.db.select('misc_settings', 'setting_value',
                                     'setting_name=?', ('status', ))
        statuses = {
            'online': discord.Status.online,
            'offline': discord.Status.offline,
            'idle': discord.Status.idle,
            'dnd': discord.Status.dnd
        }

        if status_text not in statuses.keys():
            new_status = None
        else:
            new_status = statuses[status_text]
        await self.bot.change_presence(
            activity=new_activity, status=new_status)
        self.logger.info(f'Activity changed to {activity_text}')

    @commands.command(name='changestatus')
    @commands.is_owner()
    async def _changestatus_command(self, ctx, status_text: str = None):
        statuses = {
            'online': discord.Status.online,
            'offline': discord.Status.offline,
            'idle': discord.Status.idle,
            'dnd': discord.Status.dnd
        }
        if not status_text in statuses.keys():
            return

        new_status = statuses[status_text]
        self.db.insert('misc_settings', ('status', status_text), 'REPLACE')
        activity_text = self.db.select('misc_settings', 'setting_value',
                                       'setting_name=?', ('activity', ))

        if activity_text is None or 'None' in activity_text:
            new_activity = None
        else:
            new_activity = discord.Game(activity_text)
        await self.bot.change_presence(
            activity=new_activity, status=new_status)
        self.logger.info(f'Status changed to {status_text}')

    @commands.command(name='exit')
    @commands.is_owner()
    async def _exit_command(self, ctx):
        await self.bot.logout()
        self.logger.info('***** EXITED')
        self.db.close()
        sys_exit()
        return


def setup(bot):
    bot.add_cog(OwnerCog(bot, 'Owner'))

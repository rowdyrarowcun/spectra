import discord
from discord.ext import commands

from db_manager import DBManager
from spectra_cog import SpectraCog
from utility import load_toml_file


class EventResponseModule:
    def __init__(self, module_name, channel_mode, module_type, response_mode,
                 response_channel):
        self.module_name = module_name
        self.channel_mode = channel_mode
        self.module_type = module_type
        self.response_mode = response_mode
        self.response_channel = response_channel


class EventResponse(SpectraCog):

    channel_events = [
        'message', 'message_delete', 'message_edit', 'reaction_add',
        'reaction_remove', 'reaction_clear', 'guild_channel_update',
        'guild_channel_pins_update'
    ]

    nonchannel_events = [
        'guild_channel_delete', 'guild_channel_create',
        'guild_integrations_update', 'webhooks_update', 'member_join',
        'member_remove', 'member_update', 'guild_update', 'guild_role_create',
        'guild_role_delete', 'guild_role_update', 'guild_emojis_update',
        'voice_state_update', 'member_ban', 'member_unban'
    ]

    channel_modes = ['blacklist', 'whitelist', 'all', 'none']

    def create_event_response_channels_table(self):
        fields = [
            'guild_id INTEGER NOT NULL', 'channel_id INTEGER NOT NULL',
            'module_name TEXT NOT NULL',
            'PRIMARY KEY (guild_id, module_name, channel_id)'
        ]
        return self.db.create_table(
            'er_channels', fields, (), if_not_exists=True, rowid=False)

    def create_event_response_modules_table(self):
        fields = [
            'guild_id INTEGER NOT NULL', 'module_name TEXT NOT NULL',
            'event TEXT', 'channel_mode TEXT', 'module_type TEXT',
            'response_mode TEXT', 'response_channel TEXT',
            'PRIMARY KEY (guild_id, module_name)'
        ]
        return self.db.create_table(
            'er_modules', fields, (), if_not_exists=True, rowid=False)

    def create_event_response_bulletins_table(self):
        fields = [
            'guild_id INTEGER NOT NULL', 'module_name TEXT NOT NULL',
            'channel_mode TEXT', 'selfreact INTEGER',
            'threshold INTEGER NOT NULL', 'PRIMARY KEY (guild_id, module_name)'
        ]
        return self.db.create_table(
            'er_bulletins', fields, (), if_not_exists=True, rowid=False)

    def create_insert_triggers(self):
        if not self.db.table_exists('er_modules'):
            self.logger.error(
                'Tried to create trigger while er_modules does not exist')
            return False
        if not self.db.table_exists('er_bulletins'):
            self.logger.error(
                'Tried to create trigger while er_bulletins does not exist')
            return False

        modules_trigger_query = """
            CREATE TRIGGER IF NOT EXISTS modules_trigger
            BEFORE INSERT ON er_modules
            FOR EACH ROW
            BEGIN
                SELECT RAISE (ABORT, "Module already declared as bulletin")
                WHERE EXISTS (SELECT 1 FROM er_bulletins
                WHERE guild_id=NEW.guild_id AND module_name=NEW.module_name); 
            END
                """
        bulletins_trigger_query = """
            CREATE TRIGGER IF NOT EXISTS bulletins_trigger
            BEFORE INSERT ON er_bulletins
            FOR EACH ROW
            BEGIN
                SELECT RAISE (ABORT, "Bulletin already declared as module")
                WHERE EXISTS (SELECT 1 FROM er_modules
                WHERE guild_id=NEW.guild_id AND module_name=NEW.module_name); 
            END
                """
        self.db.execute(modules_trigger_query)
        self.db.execute(bulletins_trigger_query)

    def __init__(self, bot, cog_name):
        super().__init__(bot, cog_name)
        self.create_event_response_channels_table()
        self.create_event_response_modules_table()
        self.create_event_response_bulletins_table()
        self.create_insert_triggers()


def setup(bot):
    bot.add_cog(EventResponse(bot, 'EventResponse'))

import discord
from discord.ext import commands

from spectra_cog import SpectraCog


class AdminCog(SpectraCog):
    @commands.command(name='setprefix')
    async def _setprefix_command(self, ctx, prefix: str):
        guild_id = ctx.message.guild.id
        self.db.update('guilds', 'prefix=?', 'guild_id=?', (prefix, guild_id))
        return


def setup(bot):
    bot.add_cog(AdminCog(bot, 'Admin'))
